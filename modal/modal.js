export class Modal {
  constructor(selector, options) {
    this.$modal = document.querySelector(selector);
    this.$btn = document.querySelectorAll(`[data-action]`);

    this.#render();
    this.#setup()
  }

  #render() {
    this.$body = document.querySelector('body');
  }

  #setup() {
    this.clickOpenModal = this.clickOpenModal.bind(this);
    this.$btn.forEach(btn => {
      btn.addEventListener('click', this.clickOpenModal)
    })
  }

  open() {
    this.$body.classList.add('modal-open');
  }

  close() {
    this.$body.classList.remove('modal-open');
  }

  clickOpenModal(event) {
    const {action} = event.target.dataset;

    if (action === 'open') {
      this.open();
    } else if (action === 'close') {
      this.close();
    }
  }

}
